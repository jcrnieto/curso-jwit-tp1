import React from 'react';
import styled from 'styled-components';
import check from '../images/check.png';


const Container = styled.div `
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p `
    color: white;
    margin:0px;
    font-weight: 500;
    text-transform: uppercase;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

 export const Circulo = styled.div `
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props=>props.visible? 'white': 'tranparente' };
`;

export const Icon = styled.img `
  height: 35px;
  width: 35px;
  object-fit: contain;
  border-radius: 50%;
  visibility: ${props=>props.visible ? 'visible' : 'hidden'};
`;


function Options({title, state, onChange}) {

    const handleChange = () => {

        if (typeof onChange === 'function') onChange(title)

    };
     
return (
     <Container>

        <Title>
            { title }
        </Title>

        <Circulo visible={state} onClick={handleChange}>
              <Icon src={check} visible={state}/>
        </Circulo>

     </Container>
  )
}

export default Options