import './App.css';
import React, { useState } from 'react';
import styled from 'styled-components';
import Card from './Card';
import Index from './submenu/index';



const Container = styled.div `
  
`;

function App() {

  const [data, setData] = useState( [
    {
        id: 1,
        color:'#21d0d0',
        options:[
    { title: 'PRICE LOW TO HIGH', state: false},
    { title: 'PRICE HIGH TO LOW', state: false},
    { title: 'popularity', state: false},
    ],
    footer: 2
    },
    {
      id:2,
      color:'#ff7745',
      options:[
  { title: 'lup nutrition', state: false},
  { title: 'asitis', state: false},
  { title: 'avvatar', state: false},
  { title: 'big muscles', state: false},
  { title: 'bpi sports', state: false},
  { title: 'bsn', state: false},
  { title: 'cellucor', state:false},
  { title: 'domin8r', state: false},
  ],
  footer: 1
  },
])

  

const handleChange = (value, id) => {

 
 const match = data.filter(el=>el.id === id)[0]//filtro por id el cuadro que clickeo

 const act = match.options.filter(el=>el.title===value)[0];//devuelvo el nombre perteneciente al circulo que clickié

 const copy = [...match.options]

 const targetIndex = copy.findIndex(f => f.title === value);

 if(targetIndex > -1 ) {

  copy[targetIndex] =  {title: value, state: !act.state}

 // console.log( copy)

  match.options = copy;

  };

  const copyList = [...data];

 const targetIndexList = copyList.findIndex(f => f.title === id);

 if (targetIndexList > -1 ) {

  copy[targetIndexList] =  match;

 }

 setData (copyList)
  
 }

 const handleReset = (id) => {

  const copy = [...data];

  const targetIndex = copy.findIndex(f => f.id === id);
 
  if (targetIndex > -1 ) {

   var raw = []; 

   const options = data.filter(el => el.id === id)[0].options 

   for (let x of options) raw.push({ title: x.title, state: false })
 
   copy[targetIndex].options = raw;
 
  }
 
  setData (copy)

 }

 

  return (
    <Container>
      <Card />
      
      {
        data.map((v, i)=> <Index key={i} {...v} onChange={handleChange} onReset={handleReset}/>)
      }
     
    </Container>     
    
  );
}

export default App;
