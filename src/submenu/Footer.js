import React from 'react';
import styled from 'styled-components';
import {Circulo, Icon} from './Options';
import check from '../images/check.png';
import cancel from '../images/cancel.png';
import reset from '../images/reset.png';

const ContainerV1 = styled.div `
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: row;
   margin-top: 15px;
   padding-top: 25px;
   padding-bottom: 25px;
   border-Top: 1px solid #ffffff80;
`;

const ContainerV2 = styled.div `
   display: flex;
   justify-content: center;
   align-items: center;
   flex-direction: row;
`;

const Item = ({v, onClick}) => {
    return(
     <Circulo visible={true} style={{marginLeft: 15, height:60, width:60}} onClick={onClick}>
       <Icon visible={true} src={v} style={{ height:60, width:60}} />
     </Circulo>
    )
 }



function Footer({version, onReset, onCancel, onAcept}) {

    const handleReset = () => {
        console.log('vamos a resetear')
        if (typeof onReset === 'function') onReset()
    }

    const handleOk = () => {
        console.log('vamos a darle ok')
        if (typeof onReset === 'function') onAcept()
    }

    const handleCancel = () => {
        console.log('vamos a cancelar')
        if (typeof onReset === 'function') onCancel()
    }

     if(version === 1){

        let data = [
            { icon: cancel, onClick: handleCancel},
            { icon: reset, onClick: handleReset},
            { icon: check, onClick: handleOk}
        ]
        
            return (
                <ContainerV1>
           
                       {
                           data.map((v,i)=> <Item key={i} v={v.icon} onClick={v.onClick}/>)
                       }
                </ContainerV1>
           
            );
        }

      if(version === 2){
            
        let data = [
            { icon: cancel, onClick: handleCancel},
            { icon: check, onClick: handleOk}
        ]
            return (
                <ContainerV2 >
           
                       {
                           data.map((v,i)=> <Item v={v.icon} onClick={v.onClick} />)
                       }
           
                </ContainerV2>
           
            );
          

       
    }
   
      
};

export default Footer
