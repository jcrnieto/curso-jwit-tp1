import React from 'react';
import styled from 'styled-components';
import Options from './Options';
import Footer from './Footer';

const Container = styled.div `
   margin-top: 15px;
   background-color: ${props=>props.color};
   border-radius: 25px;
   width: 400px;
   padding-top: 15px;
`;



function index(params) {

  const handleChange = (value) => {
    if (typeof params.onChange === 'function') params.onChange(value, params.id);
    // console.log(value)
  }

  const handleReset = () => {
    if (typeof params.onReset === 'function') params.onReset(params.id);
    // console.log(value)
  }

  return (
     <Container color={params.color}>

          
            { 
            params.options.map((v, i)=> <Options key={i} {...v} onChange={handleChange} />)

            }
           
           <Footer version={params.footer} onReset={handleReset} /*onReset onCancel onAcept*/  />

          

     </Container>
  );
};

export default index
